# Immediate Refocus
This DLL prevents the game from destroying the InputManager when focus is lost.  As a result, the game does not recreate it when focus is gained.  This stops the game from momentarily freezing when focusing the window.  If you are playing in fullscreen mode, the game will momentarily freeze for other reasons that this DLL does not fix.

## Usage
Use the latest version of [RedBlocklandLoader](https://gitlab.com/Eagle517/redblocklandloader/-/releases) to easily load the DLL into the game.

## Building
This DLL relies on [TSFuncs](https://gitlab.com/Eagle517/tsfuncs) which you will need to build first.  Building with CMake is straightforward, although it has only been setup for MinGW.  A toolchain has been included for cross-compiling on Linux.
