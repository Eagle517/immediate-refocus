#include <windows.h>

#include "TSHooks.hpp"

/* location of JZ opcode that checks if the DInputManager exists
   during window defocus in WM_ACTIVATEAPP of WindowProc */
ADDR patchLoc;

bool init()
{
	BlInit;

	BlScanHex(patchLoc, "74 0B 80 79 4C 00 74 05");

	// change to JMP short so it always happens, skips destroying the input manager
	BlPatchByte(patchLoc, 0xEB);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");
	return true;
}

bool deinit()
{
	BlPatchByte(patchLoc, 0x74); // revert back to JZ
	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
